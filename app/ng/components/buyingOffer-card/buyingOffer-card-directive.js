'use strict';

angular.module('myApp.buyingoffers')

    .directive('mvBuyingofferCard', function() {
        return {
            restrict: 'A',
            scope: {
                buyingoffer: '='
            },
            templateUrl: 'components/buyingoffer-card/buyingoffer-card.html'
        };
    });
