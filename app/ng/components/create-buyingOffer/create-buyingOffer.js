angular.module('myApp.buyingoffers')
    .controller('CreateBuyingofferCtrl', function($scope, Buyingoffer, $mdDialog, $rootScope, currUser) {

        $scope.buyingoffer = new Buyingoffer();
        $scope.conditions = [{
            abbr: "Like new",
            short_desc: "G – General Audiences",
            long_desc: "All ages admitted. Nothing that would offend parents for viewing by children."
        },
            {
                abbr: "Light Scratches",
                short_desc: "PG – Parental Guidance Suggested",
                long_desc: "Some material may not be suitable for children. Parents urged to give \"parental guidance\". May contain some material parents might not like for their young children."
            },
            {
                abbr: "Heavy Scratches",
                short_desc: "PG-13 – Parents Strongly Cautioned",
                long_desc: "Some material may be inappropriate for children under 13. Parents are urged to be cautious. Some material may be inappropriate for pre-teenagers."
            },
            {
                abbr: "Broken",
                short_desc: "R – Restricted",
                long_desc: "Under 17 requires accompanying parent or adult guardian. Contains some adult material. Parents are urged to learn more about the film before taking their young children with them."
            }];
        $scope.price = 0;





        $scope.save = function() {
            $scope.buyingoffer.user = currUser.getUser()._id;
            $scope.buyingoffer.$save()
                .then(function(){
                    $rootScope.$broadcast('buyingofferCreated', $scope.buyingoffer);
                    $mdDialog.hide(true);
                }).catch(function(){
                $mdDialog.hide(false);
            });
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

    });