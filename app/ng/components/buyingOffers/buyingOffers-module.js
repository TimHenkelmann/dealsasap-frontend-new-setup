angular.module('myApp.buyingoffers', ['ngResource', 'ui.router'])

.config(function ($stateProvider, $urlRouterProvider, buyingofferDetailsState, buyingofferListState) {
    $stateProvider

        .state('buyingoffers', {

            // With abstract set to true, that means this state can not be explicitly activated.
            // It can only be implicitly activated by activating one of its children.
            abstract: true,
            parent: 'root',

            // This abstract state will prepend '/buyingoffers' onto the urls of all its children.
            url: '/buyingoffers'

        })


        // Using a '.' within a state name declares a child within a parent.
        // So you have a new state 'list' within the parent 'buyingoffers' state.
        .state(buyingofferListState.name, buyingofferListState.options)

        .state(buyingofferDetailsState.name, buyingofferDetailsState.options);

});





