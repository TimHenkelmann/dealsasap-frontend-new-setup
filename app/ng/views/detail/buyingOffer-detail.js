'use strict';

angular.module('myApp.buyingoffers')

    .constant('buyingofferDetailsState', {
        name: 'buyingoffers.detail',
        options: {
            url: '/{buyingofferId}',

            views: {
                "content@root": {
                    templateUrl: 'views/detail/buyingoffer-detail.html',
                    controller: 'BuyingofferDetailCtrl'
                }
            },

            resolve: {
                //we abuse the resolve feature for eventual redirection
                redirect: function($state, $stateParams, Buyingoffer, $timeout, $q){
                    var mid = $stateParams.buyingofferId;
                    var mid = $stateParams.buyingofferId;
                    if (!mid) {
                        //timeout because the transition cannot happen from here
                        $timeout(function(){
                            $state.go("buyingoffers.list");
                        });
                        return $q.reject();
                    }
                }
            },
            ncyBreadcrumb: {
                // a bit ugly (and not stable), but ncybreadcrumbs doesn't support direct access
                // to a view controller yet if there are multiple views
                label: "{{$$childHead.$$childHead.buyingoffer.title}}",
                parent: "buyingoffers.list"
            }

        }
    })
    .controller('BuyingofferDetailCtrl', function($scope, Buyingoffer, $mdToast, $mdDialog, $stateParams, $state, currUser) {

        $scope.buyingoffer = Buyingoffer.get({buyingofferId: $stateParams.buyingofferId});

        $scope.mayDelete;
        $scope.mayEdit = currUser.loggedIn();
        $scope.deleteBuyingoffer = deleteBuyingoffer;
        $scope.updateBuyingoffer = updateBuyingoffer;
        $scope.cancelEditingBuyingoffer = function(){ showSimpleToast("Editing cancelled"); }

        $scope.buyingoffer.$promise.then(function(){
            $scope.mayDelete = $scope.buyingoffer.user && $scope.buyingoffer.user == currUser.getUser()._id;
        });

        $scope.$watch(function(){
            return currUser.loggedIn();
        }, function(loggedIn){
            if (!loggedIn) {
                $scope.mayDelete = false;
                $scope.mayEdit = false;
            } else {
                $scope.mayEdit = true;
                $scope.mayDelete = $scope.buyingoffer.user == currUser.getUser()._id;
            }
        });

        ////////////////////


        function updateBuyingoffer(changed) {

            if (!changed) {
                showSimpleToast("no change");
                return;
            }

            $scope.buyingoffer.$update().then(function(updated){
                $scope.buyingoffer = updated;
                showSimpleToast("update successfull");
            }, function(){
                showSimpleToast("error. please try again later");
            });
        }

        function deleteBuyingoffer(ev) {

            var confirm = $mdDialog.confirm()
                .title('Are you sure you want to delete this buying offer?')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('Abort');

            var toastText;
            $mdDialog.show(confirm).then(function() {
                return $scope.buyingoffer.$remove().then(function() {
                    return $state.go('buyingoffers.list');
                }).then(function(){
                    showSimpleToast('Buying offer deleted successfully');
                }, function() {
                    showSimpleToast("Error. Try again later");
                });
            }, function() {
                showSimpleToast("delete aborted");
            })
        }

        function showSimpleToast(txt) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(txt)
                    .position('bottom right')
                    .hideDelay(3000)
            );
        }


    });