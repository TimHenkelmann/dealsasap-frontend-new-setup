'use strict';

angular.module('myApp.buyingoffers')

    .constant('buyingofferListState', {
        name: 'buyingoffers.list',
        options: {

            // Using an empty url means that this child state will become active
            // when its parent's url is navigated to. Urls of child states are
            // automatically appended to the urls of their parent. So this state's
            // url is '/buyingoffers' (because '/buyingoffers' + '').
            url: '',

            views: {
                'content@root': {
                    templateUrl: 'views/list/buyingoffer-list.html',
                    controller: 'BuyingofferListCtrl',
                },
                'outside@root': {
                    templateUrl: 'views/list/buyingoffer-list-buttons.html',
                    controller: 'buyingofferListButtonCtrl'
                }
            },

            ncyBreadcrumb: {
                label: "Buyingoffers"
            }

        }

    })

    .controller('BuyingofferListCtrl', function($scope, Buyingoffer) {
        $scope.buyingoffers = Buyingoffer.query();

        $scope.$on('buyingofferCreated', function(ev, buyingoffer){
            $scope.buyingoffers.push(buyingoffer);
        });


    })

    .controller('buyingofferListButtonCtrl', function($scope, $mdMedia, $mdDialog, $mdToast, currUser){

        $scope.createBuyingofferDialog = createBuyingofferDialog;
        $scope.authed = false;

        $scope.$watch(function(){
            return currUser.loggedIn();
        }, function(loggedIn){
            $scope.authed = loggedIn;
        });

        ////////////////////////////////////

        function createBuyingofferDialog(ev) {
            var useFullScreen = ( $mdMedia('xs'));
            $mdDialog.show({
                    controller: "CreateBuyingofferCtrl",
                    templateUrl: 'components/create-buyingoffer/create-buyingoffer.html',
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen,
                    preserveScope:true
                })
                .then(function(answer) {

                    if (answer) {
                        showSimpleToast('Buyingoffer saved successfully');
                    } else {
                        showSimpleToast('An Error occured!');
                    }
                }, function() {
                    showSimpleToast('Buyingoffer creation cancelled');
                });

        }

        function showSimpleToast(txt){
            $mdToast.show(
                $mdToast.simple()
                    .textContent(txt)
                    .position('bottom right')
                    .hideDelay(3000)

            );
        }
    });